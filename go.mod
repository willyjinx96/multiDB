module main

go 1.17

require (
	github.com/go-logfmt/logfmt v0.5.1 // indirect se descarga automaticamente al instalar godror
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/godror/godror v0.32.1 // indirect
	github.com/godror/knownpb v0.1.0 // indirect se descarga automaticamente al instalar godror
	github.com/sijms/go-ora v1.2.1 // indirect libreria para oracle db que no se esta usando
	google.golang.org/protobuf v1.27.1 // indirect se descarga automaticamente al instalar godror
)

require (
	github.com/denisenkom/go-mssqldb v0.12.0 // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.0.0-20170517235910-f1bb20e5a188 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/sijms/go-ora/v2 v2.4.14 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
)
