package models

import (
	"errors"
	"fmt"
	"main/dbconnection"
	"main/dbconnection/oracledb"
)

type Persona struct {
	Id     int    `json:"id"`
	Ci     int    `json:"ci"`
	Nombre string `json:"nombre"`
	Sexo   string `json:"sexo"`
}

type Personas []Persona

func (persona *Persona) AddNewPersona() {

	//dbconnection.ExecuteExec(postgres.GetDB(), `insert into persona(ci, nombre, sexo) values($1,$2,$3)`, persona.Ci, persona.Nombre, persona.Sexo)
	dbconnection.ExecuteExec(oracledb.GetDB(), `insert into persona(ci, nombre, sexo) values(:1,:2,:3)`, persona.Ci, persona.Nombre, persona.Sexo)
	//dbconnection.ExecuteExec(mysql.GetDB(), `insert into persona(ci, nombre, sexo) values(?,?,?)`, persona.Ci, persona.Nombre, persona.Sexo)
	//dbconnection.ExecuteExec(mssql.GetDB(), `insert into persona(ci, nombre, sexo) values(?,?,?);`, persona.Ci, persona.Nombre, persona.Sexo)
}

func GetPersona(ci int) (*Persona, error) {
	var persona = &Persona{} //& es para asignara directamente a direccion de memoria, si no lo toma como variable global
	//row, err := dbconnection.ExecuteQuery(postgres.GetDB(), "select * from persona where ci = $1", ci)
	//row, err := dbconnection.ExecuteQuery(mssql.GetDB(), "select * from persona where ci = ?", ci)
	//row, err := dbconnection.ExecuteQuery(mysql.GetDB(), "select * from persona where ci = ?", ci)
	row, err := dbconnection.ExecuteQuery(oracledb.GetDB(), "select * from persona where ci = :1", ci)
	if err != nil {
		fmt.Println("Error de ...")
		return nil, err
	}

	defer row.Close() //limpia el bufer

	if row.Next() {
		err = row.Scan(&persona.Id, &persona.Ci, &persona.Nombre, &persona.Sexo) //leer lo que devolvio

		if err != nil {
			fmt.Println("Error al leer...")
		}

		return persona, err
	}
	return nil, errors.New("no se encuentra la persona")
}

func GetAllPersonas() (Personas, error) {

	var personas = Personas{}

	//row, err := dbconnection.ExecuteQuery(postgres.GetDB(), "select * from persona")
	//row, err := dbconnection.ExecuteQuery(mssql.GetDB(), "select * from persona")
	//row, err := dbconnection.ExecuteQuery(mysql.GetDB(), "select * from persona")
	row, err := dbconnection.ExecuteQuery(oracledb.GetDB(), "select * from persona")
	if err != nil {
		fmt.Println("Error de ...")
		return nil, err
	}

	defer row.Close() //limpia el bufer

	for row.Next() {
		var persona = Persona{}
		err = row.Scan(&persona.Id, &persona.Ci, &persona.Nombre, &persona.Sexo) //leer lo que devolvio

		if err != nil {
			fmt.Println("Error al leer...")
		}

		personas = append(personas, persona)
	}
	return personas, err
}
func (persona *Persona) UpdatePersona(id int) {
	//dbconnection.ExecuteExec(postgres.GetDB(), `update persona set ci=$1, nombre=$2, sexo=$3 where id=$4`, persona.Ci, persona.Nombre, persona.Sexo, id)
	//dbconnection.ExecuteExec(mssql.GetDB(), `update persona set ci=?, nombre=?, sexo=? where id=?`, persona.Ci, persona.Nombre, persona.Sexo, id)
	//dbconnection.ExecuteExec(mysql.GetDB(), `update persona set ci=?, nombre=?, sexo=? where id=?`, persona.Ci, persona.Nombre, persona.Sexo, id)
	dbconnection.ExecuteExec(oracledb.GetDB(), `update persona set ci=:1, nombre=:2, sexo=:3 where id=:4`, persona.Ci, persona.Nombre, persona.Sexo, id)
}

func DeletePersona(ci int) (string, error) {
	//row, err := dbconnection.ExecuteQuery(postgres.GetDB(), "delete from persona where ci = $1", ci)
	//row, err := dbconnection.ExecuteQuery(mssql.GetDB(), "delete from persona where ci = ?", ci)
	//row, err := dbconnection.ExecuteQuery(mysql.GetDB(), "delete from persona where ci = ?", ci)
	row, err := dbconnection.ExecuteQuery(oracledb.GetDB(), "delete from persona where ci = :1", ci)

	if err != nil {
		fmt.Println("Error al eliminar...", err)
	}
	defer row.Close()
	//fmt.Println("se elimino el registro")
	return "Eliminado", err
}
