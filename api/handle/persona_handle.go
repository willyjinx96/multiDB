package handle

import (
	"encoding/json"
	"fmt"
	"main/api/models"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddNewPersona(w http.ResponseWriter, r *http.Request) {

	persona := models.Persona{} //modelo del obj

	decoder := json.NewDecoder(r.Body) //decodificando el body enviado

	defer r.Body.Close() //cierre el canal y limpia el bufer

	if err := decoder.Decode(&persona); err != nil { //decodifica y lo guarda en el modelo persona
		fmt.Fprintf(w, "Error al leer el JSON...") //envia mensaje al cliente
	}
	fmt.Println(persona)

	persona.AddNewPersona()
}

func GetPersona(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	//fmt.Println("http://" + r.Host + r.URL.Path)
	ci, err := strconv.Atoi(vars["ci"])
	if err != nil {
		fmt.Println("Caracter desconocido")
	}
	persona, err := models.GetPersona(ci)
	if err != nil {
		fmt.Println("Caracter desconocido")
	}
	fmt.Println(models.GetPersona(ci))

	//w.Header().Set("Content-Type", "application/json") //permiso para esperar un JSON
	output, _ := json.Marshal(persona)   //Convierte el obj a JSON
	fmt.Fprintf(w, "%s", string(output)) //Envia al cliente w cualquier objeto
	//fmt.Println(vars["ci"])
}

func GetAllPersonas(w http.ResponseWriter, r *http.Request) {

	//fmt.Println("http://" + r.Host + r.URL.Path)
	personas, err := models.GetAllPersonas()
	if err != nil {
		fmt.Println("Caracter desconocido")
	}
	//fmt.Println(models.GetPersona(ci))

	//w.Header().Set("Content-Type", "application/json") //permiso para esperar un JSON
	output, _ := json.Marshal(personas)  //Convierte el obj a JSON
	fmt.Fprintf(w, "%s", string(output)) //Envia al cliente w cualquier objeto
}

func UpdatePersona(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	//fmt.Println("http://" + r.Host + r.URL.Path)
	id, err := strconv.Atoi(vars["id"])
	//fmt.Println(ci)
	if err != nil {
		fmt.Println("Caracter desconocido")
	}
	persona := models.Persona{} //modelo del obj

	decoder := json.NewDecoder(r.Body) //decodificando el body enviado

	defer r.Body.Close() //cierre el canal y limpia el bufer

	if err := decoder.Decode(&persona); err != nil { //decodifica y lo guarda en el modelo persona
		fmt.Fprintf(w, "Error al leer el JSON...") //envia mensaje al cliente
	}
	fmt.Println(persona)

	persona.UpdatePersona(id)
}

func DeletePersona(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	//fmt.Println("http://" + r.Host + r.URL.Path)
	ci, err := strconv.Atoi(vars["ci"])
	//fmt.Println(ci)
	if err != nil {
		fmt.Println("Caracter desconocido")
	}
	mensaje, err := models.DeletePersona(ci)
	if err != nil {
		fmt.Println("error de eliminacion")
	}
	output, _ := json.Marshal(mensaje) //Convierte el obj a JSON
	fmt.Fprintf(w, "%s", string(output))
}
