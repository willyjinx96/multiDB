package mssql

import (
	"database/sql"
	"flag"
	"fmt"
	"log"

	_ "github.com/denisenkom/go-mssqldb"
)

var db *sql.DB

func GetDB() *sql.DB {
	return db
}

var (
	debug         = flag.Bool("debug", true, "enable debugging")
	password      = flag.String("password", "msSql123", "the database password")
	port     *int = flag.Int("port", 1434, "the database port")
	server        = flag.String("server", "localhost", "the database server")
	user          = flag.String("user", "sa", "the database user")
)

func Connection() {
	var err error
	flag.Parse()

	if *debug {
		fmt.Printf(" password:%s\n", *password)
		fmt.Printf(" port:%d\n", *port)
		fmt.Printf(" server:%s\n", *server)
		fmt.Printf(" user:%s\n", *user)
	}

	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=db_prueba", *server, *user, *password, *port)
	if *debug {
		fmt.Printf(" connString:%s\n", connString)
	}

	db, err = sql.Open("mssql", connString)
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
}

func Ping() {
	err := db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("Ping a la base de datos mssql exitoso")
}
