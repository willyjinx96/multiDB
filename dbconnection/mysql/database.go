package mysql

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql" //driver de la base de datos usar "go get -u github.com/go-sql-driver/mysql"
)

var db *sql.DB

const username string = "root"
const password string = "123456"
const host string = "localhost"
const port int = 3306
const database string = "db_prueba"

func GetDB() *sql.DB {
	return db
}

func Connection() {
	connection, err := sql.Open("mysql", generateURL())
	if err != nil {
		panic(err)
	} else {
		db = connection
		fmt.Println("Conexión a la base de datos MySQL establecida")
	}
}

func Ping() {
	err := db.Ping()
	if err != nil {
		panic(err)
	}
}

func generateURL() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8", username, password, host, port, database)
}
