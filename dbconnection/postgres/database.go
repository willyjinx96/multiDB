package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "123"
	dbname   = "db_prueba"
)

var db *sql.DB

func GetDB() *sql.DB {
	return db
}

func Connection() {
	var err error
	db, err = sql.Open("postgres", generateURL())
	if err != nil {
		panic(err)
	}
	fmt.Println("Conexión a la base de datos establecida")
}

func Ping() {
	err := db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("Ping a la base de datos Postgres exitoso")
}

func generateURL() string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
}
