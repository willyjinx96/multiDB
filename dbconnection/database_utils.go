package dbconnection

import (
	"database/sql"
	"fmt"
)

func ExecuteExec(db *sql.DB, schema string, args ...interface{}) {

	_, err := db.Exec(schema, args...)
	if err != nil {
		fmt.Println(err)
	}
}

func ExecuteQuery(db *sql.DB, schema string, args ...interface{}) (*sql.Rows, error) {
	rows, err := db.Query(schema, args...)

	return rows, err
}

func CloseConnection(db *sql.DB) {
	fmt.Println("Conexion cerrada...")
	db.Close()
}
