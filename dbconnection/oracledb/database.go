package oracledb

import (
	"database/sql"
	"fmt"

	_ "github.com/sijms/go-ora/v2"
	go_ora "github.com/sijms/go-ora/v2"
)

var user = "db_prueba"
var password = "123456"
var host = "localhost"
var port = "1521"
var service_name = "XE"

var db *sql.DB

func GetDB() *sql.DB {
	return db
}

func generateCad() string {
	return fmt.Sprintf(`(DESCRIPTION=
		(ADDRESS_LIST=
			(LOAD_BALANCE=OFF)
			(FAILOVER=ON)
			(address=(protocol=tcp)(host=%s)(port=%s))
		)
		(CONNECT_DATA=
			(SERVICE_NAME=%s)
			(SERVER=DEDICATED)
		)
		(SOURCE_ROUTE=yes)
		)`, host, port, service_name)
}

func Connection() {
	var err error
	con := go_ora.BuildJDBC(user, password, generateCad(), nil)
	db, err = sql.Open("oracle", con)
	if err != nil {
		fmt.Println("Error al conectar a la base de datos de Oracle")
	} else {
		fmt.Println("Conexión a la base de datos Oracle establecida")
	}

}

func Ping() {
	if db != nil {
		err := db.Ping()
		if err != nil {
			panic(err)
		}
		fmt.Println("Ping a la base de datos Oracle exitoso")

	}
}

//PASOS PARA OBTENER EL SERVICE NAME DE ORACLE DATABASES
/*
1.- Acceder a la base con sqlplus:
		sqlplus sys/Oradoc_db1@ORCLCDB as sysdba
		donde sys es el usuario, Oradoc_db1 es el password, y ORCLCDB es la SID suele tener por defecto XE cuando no es docker
2.-Para listar los service_name:
		select value from v$parameter where name like '%service_name%';
*/

//PARA CREAR UN USUARIO EN ORACLE DATABASE
/*
ingresar lo siguiente:
	CREATE USER <user_name> IDENTIFIED BY <password>;
	ejemplo:
	CREATE USER chris833 IDENTIFIED BY pelota12;
*/
