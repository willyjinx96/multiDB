console.log("index.js");
var personas = [];
var datosOmitir = new Map();
var tablaMostrar = new TablasDinamicas(
  "cont-table",
  personas,
  "Personas",
  "personas",
  datosOmitir,
  []
);
tablaMostrar.crearTablaPorObjeto();

function getAllPersonas() {
  fetch("/api/v1/personas", {
    method: "GET",
    //body:jsonbody,
  })
    .then((response) => response.json())
    .then((data) => {
      personas = data;
      console.log(personas);
      tablaMostrar.setRealObjeto(personas);
      tablaMostrar.puntero = 0;
      tablaMostrar.actualizarTabla();
    })
    .catch(function (error) {
      //alert("Error al guardar");
      console.log(error);
    });
}

function guardarPersona() {
  var ci = document.getElementById("ci").value;
  var nombre = document.getElementById("nombre").value;
  var sexo = document.getElementById("sexo").value;
  var jsonbody = JSON.stringify({
    ci: parseInt(ci),
    nombre: nombre,
    sexo: sexo,
  });
  console.log(jsonbody);
  
  fetch("/api/v1/personas", {
    method: "POST",
    body: jsonbody,
  })
    .then(function (response) {
      alert("Persona guardada");
    })
    .catch(function (error) {
      alert("Error al guardar");
    });
}
