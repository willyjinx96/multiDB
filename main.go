package main

import (
	"fmt"

	"main/api/handle" //creacion de endpoints
	"main/dbconnection/oracledb"

	"main/dbconnection" // conexion db

	"main/templates" //montar lo estaticos del frontend
	"net/http"       //peticiones http

	"github.com/gorilla/mux" //peticiones http libreria externa
)

func main() {
	//Llama al router de Mux
	mux := mux.NewRouter()

	staticfiles := http.FileServer(http.Dir("./template/static")) // creando un  server de archivos estaticos dad una direccion

	newtemp := templates.Temp{Path: "./template/index.html"}

	mux.PathPrefix("/static/").Handler(http.StripPrefix("/static/", staticfiles))
	mux.HandleFunc("/", newtemp.RenderTemplate) //Renderiza al principio

	oracledb.Connection()
	oracledb.Ping()

	//postgres.Connection()
	//postgres.Ping()

	//mysql.Connection()
	//mysql.Ping()

	//mssql.Connection()
	//mssql.Ping()

	//Endpoints

	mux.HandleFunc("/api/v1/personas", handle.AddNewPersona).Methods("POST")               //crear
	mux.HandleFunc("/api/v1/personas/{ci:[0-9]+}", handle.GetPersona).Methods("GET")       //leer 1
	mux.HandleFunc("/api/v1/personas/{ci:[0-9]+}", handle.DeletePersona).Methods("DELETE") //eliminar
	mux.HandleFunc("/api/v1/personas/{id:[0-9]+}", handle.UpdatePersona).Methods("PUT")    //actualizar
	mux.HandleFunc("/api/v1/personas", handle.GetAllPersonas).Methods("GET")               //leer varios

	defer dbconnection.CloseConnection(oracledb.GetDB())
	//defer dbconnection.CloseConnection(postgres.GetDB())
	//defer dbconnection.CloseConnection(mysql.GetDB())
	//defer dbconnection.CloseConnection(mssql.GetDB())

	//Se abre el servidor...
	fmt.Println("Servidor iniciado en el puerto 8000")

	http.ListenAndServe(":8000", mux) //montar el server
}
