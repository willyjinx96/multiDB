# MultiDB

Conexion a base de datosm Oracle, Postgres, MySQL y SQL Server

## Para el Backend

Descargar e instalar GoLang del siguiente enlace:

- [GoLang](https://go.dev/dl/)

## Para Base de Datos en Oracle

Descargar el Instant Client la version mas actual del siguiente enlace:

- [Instant Cliente for Oracle](https://www.oracle.com/es/database/technologies/instant-client/winx64-64-downloads.html)

Luego descomprimir la carpeta en la direccion `C:\oraclexe`.
Finalemente debe agregar la ruta de la carpeta al `PATH` de las variables del sistema.

## Paquetes a instalar

Las dependencias necesarias se encuentran en go mod.

### Lista de las dependencias:

- [ ] [github.com/go-sql-driver/mysql]
- [ ] [github.com/godror/godror]
- [ ] [github.com/sijms/go-ora]
- [ ] [github.com/gorilla/mux]

Si faltara alguno le dara error y le mostrara cual dependencia falta

### Ejemplo para descargar las dependencias

Para obtener las dependencias solo es necesario hacer los siguiente:

```
go get github.com/sijms/go-ora

```

Para que en otros proyectos no tenga que hacer eso seguido puede usar -u

```
go get -u github.com/gorilla/mux

```

Como ve solo es necesario poner "go get <tu url aqui> " y listo

## Configurando en Visual Studio Code

Para configurar automaticamente los Paths haga lo siguiente

```
Ctrl+Shift+p

```

Se desplegara una barra en el cual estara el simbolo de [>]

Luego debe introducir lo siguiente:

```
Go:Install/Update Tools

```

Tickee todas las opciones y pulse <Ok>

Le comenzara a actualizar las herramientas

## Extension de Visual Studio Code recomendada

Se recomienda instalar Go de Go Team at Google

### Documentacion Adicional

- [Peticiones HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
- [GoLang Documentation](https://go.dev/doc/)
- [Documentacion Gorilla Mux](https://pkg.go.dev/github.com/gorilla/mux)
- [Gorilla Mux (Leer Readme.me)](https://github.com/gorilla/mux)
- [API Rest](https://rockcontent.com/es/blog/api-rest/)
- [JSON](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON)
- [JavaScript JS](https://developer.mozilla.org/es/docs/Web/JavaScript)
- [Fetch en JavaScript](https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Using_Fetch)
- [Bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
